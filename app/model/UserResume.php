<?php


namespace app\model;


class UserResume extends BaseModel
{
    protected $name="user_resume";
    public function userInfo(){
        return $this->hasOne('UserInfo','id','user_id');
    }

}