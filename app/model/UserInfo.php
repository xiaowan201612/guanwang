<?php


namespace app\model;


use think\Model;

class UserInfo extends BaseModel
{
    protected  $name='user_info';

    public function userEducation(){
        return $this->hasMany('UserEducation','user_id','id');
    }
    public function userExperience(){
        return $this->hasMany('UserExperience','user_id','id');
    }

    public  function userResume(){
        return $this->hasMany('UserResume','user_id','id');
    }


}