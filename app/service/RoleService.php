<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\service;

use app\model\Menu;
use app\model\Role;
use app\model\RoleMenu;
use think\facade\Db;

/**
 * 角色管理-服务类
 * @author 牧羊人
 * @since 2020/11/14
 * Class RoleService
 * @package app\service
 */
class RoleService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/14
     * RoleService constructor.
     */
    public function __construct()
    {
        $this->model = new Role();
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2020/11/20
     * @author 牧羊人
     */
    public function getList()
    {
        // 初始化变量
        $map = [];
        $sort = 'id desc';
        $is_sql = 0;
        $map[]=['id','<>',1];

        // 获取参数
        $argList = func_get_args();
        if (!empty($argList)) {
            // 查询条件
            $map = (isset($argList[0]) && !empty($argList[0])) ? $argList[0] : [];
            // 排序
            $sort = (isset($argList[1]) && !empty($argList[1])) ? $argList[1] : 'id desc';
            // 是否打印SQL
            $is_sql = isset($argList[2]) ? isset($argList[2]) : 0;
        }

        // 常规查询条件
        $param = request()->param();
        if ($param) {
            // 筛选名称
            if (isset($param['name']) && $param['name']) {
                $map[] = ['name', 'like', "%{$param['name']}%"];
            }

            // 筛选标题
            if (isset($param['title']) && $param['title']) {
                $map[] = ['title', 'like', "%{$param['title']}%"];
            }

            // 筛选类型
            if (isset($param['type']) && $param['type']) {
                $map[] = ['type', '=', $param['type']];
            }

            // 筛选状态
            if (isset($param['status']) && $param['status']) {
                $map[] = ['status', '=', $param['status']];
            }

            // 手机号码
            if (isset($param['mobile']) && $param['mobile']) {
                $map[] = ['mobile', '=', $param['mobile']];
            }
        }

        // 设置查询条件
        if (is_array($map)) {
            $map[] = ['mark', '=', 1];
        } elseif ($map) {
            $map .= " AND mark=1 ";
        } else {
            $map .= " mark=1 ";
        }
        $result = $this->model->where($map)->order($sort)->page(PAGE, PERPAGE)->column("id");

        // 打印SQL
        if ($is_sql) {
            echo $this->model->getLastSql();
        }

        $list = [];
        if (is_array($result)) {
            foreach ($result as $val) {
                $info = $this->model->getInfo($val);
                $list[] = $info;
            }
        }

        //获取数据总数
        $count = $this->model->where($map)->count();

        //返回结果
        $message = array(
            "msg" => '操作成功',
            "code" => 0,
            "data" => $list,
            "count" => $count,
        );
        return $message;
    }

    /**
     * 获取角色列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 牧羊人
     * @since 2020/11/14
     */
    public function getRoleList()
    {
        $list = $this->model->where([
            ['id', '<>', 1],
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->order("sort", "asc")
            ->select()
            ->toArray();
        return message("操作成功", true, $list);
    }

    /**
     * 获取权限列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @author 牧羊人
     * @since 2020/11/20
     */
    public function getPermissionList()
    {
        // 请求参数
        $param = request()->param();
        // 角色ID
        $roleId = intval(getter($param, "role_id", 0));
        // 获取全部菜单
        $menuModel = new Menu();
        $menuList = $menuModel->where([
            ['status', '=', 1],
            ['mark', '=', 1],
        ])->order("sort", "asc")->select()->toArray();
        if (!empty($menuList)) {
            $roleMenuModel = new RoleMenu();
            $roleMenuList = $roleMenuModel
                ->field("menu_id")
                ->where("role_id", '=', $roleId)
                ->select()
                ->toArray();
            $menuIdList = array_key_value($roleMenuList, "menu_id");
            foreach ($menuList as &$val) {
                if (in_array($val['id'], $menuIdList)) {
                    $val['checked'] = true;
                    $val['open'] = true;
                }
            }
        }
        return message("操作成功", true, $menuList);
    }

    /**
     * 保存权限
     * @return array
     * @since 2020/11/14
     * @author 牧羊人
     */
    public function savePermission()
    {

        // 请求参数
        $param = request()->param();
        $ids= str_replace('[','',$param['ids']);
        $ids= explode(',',str_replace(']','',$ids));
        // 角色ID
        $roleId = intval(getter($param,'id',0));
        if (!$roleId) {
//            添加
            $roleId = Db::name('role')->insertGetId(['name' => $param['name'], 'create_time' => time(), 'update_time' => time()]);
        }else{
             Db::name('role')->where('id',$roleId)->update(['name' => $param['name'], 'update_time' => time()]);
        }

        if ($roleId > 0) {
            // 删除角色菜单关系数据
            $roleMenuModel = new RoleMenu();
            $roleMenuModel->where("role_id", $roleId)->delete();
            // 插入角色菜单关系数据
            if (is_array($ids) && !empty($ids)) {
                $list = [];
                foreach ($ids as $val) {
                    $data = [
                        'role_id' => $roleId,
                        'menu_id' => $val,
                    ];
                    $list[] = $data;
                }
            }
            $roleMenuModel->insertAll($list);
            return message('操作成功',true);
        }else{
            return message('操作失败',false);
        }
    }

}