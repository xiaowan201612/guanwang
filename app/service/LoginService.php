<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\service;

use app\model\ActionLog;
use app\model\SmsRecord;
use app\model\User;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use sms\Sms;
use think\facade\Db;

/**
 * 登录-服务类
 * @author 牧羊人
 * @since 2020/11/14
 * Class LoginService
 * @package app\service
 */
class LoginService extends BaseService
{
    protected $smsRecord;
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/11/14
     * LoginService constructor.
     */
    public function __construct()
    {
        $this->model = new User();
        $this->smsRecord = new SmsRecord();
    }

    /**
     * 获取验证码
     * @return array
     * @since 2020/11/14
     * @author 牧羊人
     */
    public function captcha()
    {
        //生成随机UID
        $uuid = get_guid_v4();

        //生成图片验证码
        $verify = new \Verify(['length' => 4, 'useCurve' => true]);
        // 验证码图片
        $img = $verify->entry($uuid);
        // 验证码值
        $code = $verify->getCode();

        // 把内容存入 cache，10分钟后过期
        $key = get_guid_v4();
        $this->model->setCache($key, $code, 10 * 60);

        // 返回结果
        $result = [
            'key' => $key,
            'captcha' => "data:image/png;base64," . base64_encode($img)
        ];
        return message("操作成功", true, $result);
    }

    /**
     * 登录系统
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @since 2020/11/15
     * @author 牧羊人
     */
    public function login()
    {
        // 请求参数
        $param = request()->param();
        // 登录账号
        $mobile = trim($param['phone']);
        if (!$mobile) {
            return message('手机号码不能为空', false);
        }
        // 登录密码
        $code = trim($param['code']);
        if (!$code) {
            return message('验证码不能为空', false);
        }
        // 默认验证码123456，验证码验证未完成
        $code_data = Db::name('sms_record')->where(['phone'=>$mobile])->order('time desc')->value('content');
//        $code_data = '123456';
        if($code !=123456 && $code != $code_data){
            return message('验证码不正确', false);
        }


        // 用户验证
        $info = $this->model->where([
            ['username', '=', $mobile],
        ])->find();
//        return $info;
        if (!$info ) {
            return message('登录账号不存在', false);
        }

        // 使用状态校验
        if ($info['status'] != 1) {
            return message("帐号已被禁用", false);
        }

        // 设置日志标题
        ActionLog::setTitle("登录系统");

        // JWT生成token
        $jwt = new \Jwt();
        $token = $jwt->getToken($info['id']);
        $ip=get_client_ip(0,true);
        $this->model->where('id',$info['id'])->update(['login_num'=>$info['login_num']+1,'login_ip'=>$ip,'login_time'=>time()]);

        // 结果返回
        $result = [
            'access_token' => $token,
        ];
        return message('登录成功', true, $result);
    }

    /**
     * 注销系统
     * @return array
     * @since 2020/11/12
     * @author 牧羊人
     */
    public function logout()
    {
//        // 清空SESSION值
//        session()->put("userId", null);
        // 记录退出日志
        ActionLog::setTitle("注销系统");
        // 创建退出日志
        ActionLog::record();
        return message();
    }

    public function get_code(){

        $phone = request()->param('phone');
        $code = rand(111111,999999);


        $sms = new Sms();
        $result = $sms->send_code($phone, $code);
        if(!empty($result)){
            $data = ['phone'=>$phone,'content'=>$code,'time'=>time()];
//        将短信验证码存储进数据库
            $res = $this->smsRecord->insert($data);
            return message('发送成功',true);
        }else{
            return message('发送失败',false);
        }
    }

}