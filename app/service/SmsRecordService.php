<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\service;


use app\model\SmsRecord;

/**
 * 短信发送记录管理-服务类
 * @author 牧羊人
 * @since: 2021/04/06
 * Class SmsRecordService
 * @package app\admin\service
 */
class SmsRecordService extends BaseService
{
    /**
     * 构造函数
     * LevelService constructor.
     */
    public function __construct()
    {
        $this->model = new SmsRecord();
    }
	
	/**
     * 获取数据列表
     * @return array
     * @since 2021/04/06
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->param();

        // 查询条件
        $map = [];
		

        return parent::getList($map); // TODO: Change the autogenerated stub
    }


                
}