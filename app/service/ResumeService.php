<?php


namespace app\service;


use app\model\UserInfo;
use app\model\UserResume;
use think\facade\Db;

class ResumeService extends BaseService
{
    /**
     * 构造函数
     * LevelService constructor.
     */
    public function __construct()
    {
        $this->model = new UserResume();
    }

    public function resumeList(){
        $param= request()->param();
        $where=[];

        if(isset($param['start_time']) && !empty($param['start_time'])){
            $resume_where[]=['UserResume.create_time','>=',strtotime($param['start_time'])];
        }
        if(isset($param['end_time']) && !empty($param['end_time'])){
            $resume_where[]=['UserResume.create_time','<',strtotime($param['end_time'].' 23:59:59')];
        }
        if(isset($param['status']) && !empty($param['status'])){
            if($param['status'] ==1){
                $resume_where[]=['status','=',5];
            }elseif ($param['status'] == 2){
                $resume_where[]=['status','=',4];
            }elseif ($param['status'] == 3){
                $resume_where[]=['status','in',[1,2,3]];
            }

        }
        if(isset($param['name']) && !empty($param['name'])){
            $where[]=['UserInfo.name','=',$param['name']];
        }
        if(isset($param['occupation_id']) && !empty($param['occupation_id'])){
            $job_ids = Db::name('job')->where('occupation_id',$param['occupation_id'])->column('id');
            $resume_where[]=['job_id','in',$job_ids];
        }

        $where[]=['UserInfo.mark','=',1];
        $resume_where[]=['UserResume.mark','=',1];

        if(isset($param['type']) && !empty($param['type'])){
            $start_time = strtotime(date('Y-m-01'));
            $end_time = strtotime(date('Y-m-d',strtotime('+1 day')));
            $resume_where[]=['UserResume.create_time','between',[$start_time,$end_time]];
        }
        $resume= $this->model->hasWhere('userInfo',$where)->with('userInfo')->where($resume_where)->page($param['page'],$param['limit'])->select();
        $count= $this->model->hasWhere('userInfo',$where)->where($resume_where)->count();
        $resume= json_decode(json_encode($resume),true);
        $return_list=[];
        if(!empty($resume)){
            $status=[0=>'已投递' ,1=>'HR面试',2=>'主管面试',3=>'总经理面试',4=>'已录用',5=>'未录用'];
            foreach ($resume as $key=>$value){
                $job= Db::name('job')->where('id',$value['job_id'])->find();

                $position_name= Db::name('occupation')->where('id',empty($job)?0:$job['occupation_id'])->find();
                $position_name=empty($position_name)?'':$position_name['name'];
                $job_city= Db::name('job_city')->where('id',empty($job)?0:$job['city_id'])->value('city','');
                $return_list[]=[
                    'id'=>$value['id'],
                    'user_id'=>$value['user_id'],
                    'nick_name'=>isset($value['userInfo']['nick_name'])?$value['userInfo']['nick_name']:'',
                    'name'=>isset($value['userInfo']['name'])?$value['userInfo']['name']:'',
                    'phone'=>isset($value['userInfo']['phone'])?$value['userInfo']['phone']:'',
                    'e_mail'=>isset($value['userInfo']['e_mail'])?$value['userInfo']['e_mail']:'',
                    'position_name'=>$position_name.'('.$job_city.')',
                    'status'=>$value['status'],
                    'status_name'=>$status[$value['status']],
                    'create_time'=>$value['create_time'],
                    'update_time'=>$value['update_time'],
                ];
            }
        }

        return message('',true,$return_list,$count);


    }

    public function readResume(){
        $param = request()->param();
        $userInfoModel = new UserInfo();
        $user_info =  $userInfoModel->with(['userEducation'=>function($query){
            $query->where('mark',1);
        },'UserExperience'=>function($query){
            $query->where('mark',1);
        }])->where('id',$param['user_id'])->find();
        $user_job_1= Db::name('user_resume')->where('mark',1)->where('status',0)->count();
        $user_job_2 =Db::name('user_resume')->where('mark',1)->whereNotIn('status',[4,5])->count();
        $user_info['is_invite']= $user_job_1 >0?1:0;
        $user_info['is_refuse']= $user_job_2 >0?1:0;
        return message('',true,$user_info);
    }

    public function userInfoList(){
        $param= request()->param();
        $where=[];
        $resume_where=[];
        $resume_where[]=['UserResume.mark','=',1];
        if(isset($param['status']) && !empty($param['status'])){
            if($param['status'] ==1){
                $resume_where[]=['status','=',5];
            }elseif ($param['status'] == 2){
                $resume_where[]=['status','=',4];
            }elseif ($param['status'] == 3){
                $resume_where[]=['status','in',[1,2,3]];
            }else{
                $user_id = Db::name('user_resume')->column('user_id');
                $where[]=['id','not in',$user_id];
            }

        }
        if(isset($param['name']) && !empty($param['name'])){
            $where[]=['UserInfo.name','=',$param['name']];
        }
        if(isset($param['occupation_id']) && !empty($param['occupation_id'])){
            $job_ids = Db::name('job')->where('occupation_id',$param['occupation_id'])->column('id');
            $resume_where[]=['job_id','in',$job_ids];
        }

        $where[]=['UserInfo.mark','=',1];


        if(isset($param['type']) && !empty($param['type'])){
            $start_time = strtotime(date('Y-m-01'));
            $end_time = strtotime(date('Y-m-d',strtotime('+1 day')));
            $resume_where[]=['UserResume.create_time','between',[$start_time,$end_time]];
        }
        $userInfoModel= new UserInfo();
        $count= $userInfoModel->hasWhere('userResume',$resume_where,'*','left')->where($where)->count();
        $user_info=$userInfoModel->hasWhere('userResume',$resume_where,'*','left')->with(['userResume'=>function($query){
                $query->order('id desc');
        }])->where($where)->page($param['page'],$param['limit'])->select();
//        print_r($userInfoModel->getLastSql());
//        die;
        $user_info= json_decode(json_encode($user_info),true);

        $return_list=[];
        if(!empty($user_info)){
            $status=[1=>'未完善',2=>'已完善'];
            foreach ($user_info as $key=>$value){
                $ocp = [];
                $ocp_status='未投递';
                if(!empty($value['userResume'])){
                    foreach ($value['userResume'] as $k=>$val){
                        if($k==0){
                            switch ($val['status']){
                                case 0:
                                    $ocp_status="已投递";
                                    break;
                                case 4:
                                    $ocp_status="已录用";
                                    break;
                                case 5:
                                    $ocp_status="未录用";
                                    break;
                                case 1||2||3:
                                    $ocp_status="面试中";
                                    break;

                            }
                        }
                        $job= Db::name('job')->where('id',$val['job_id'])->find();
                        $position_name= Db::name('occupation')->where('id',$job['occupation_id'])->value('name','');
                        $job_city= Db::name('job_city')->where('id',$job['city_id'])->value('city','');
                        $ocp[]= $position_name.(empty($job_city)?'':"({$job_city})");
                    }


                    //岗位信息


                }
                $value['resume_status']= $status[$value['resume_status']];
                $value['ocp']= empty($ocp)?'/':implode(',',$ocp);
                $value['ocp_status']=$ocp_status;
                $return_list[]=$value;
//                $job= Db::name('job')->where('id',$value['job_id'])->find();
//                $position_name= Db::name('occupation')->where('id',$job['occupation_id'])->value('name','');
//                $job_city= Db::name('job_city')->where('id',$job['city_id'])->value('city','');

            }
        }


        return message('',true,$return_list,$count);
    }

    public function updateStatus($admin_id){
        $param= request()->param();
        $data=[
            'status'=>$param['status'],
            'update_time'=>time(),
            'update_user'=>$admin_id,
        ];
        if($param['status']==1){
            $where[]=['status','=',0];

        }elseif ($param['status']==5){
            $where[]=['status','in',[0,1,2,3]];
        }else{
            return message('操作失败',false);
        }
        $info =Db::name('user_resume')->where('id',$param['id'])->where($where)->update($data);
        if($info){
            return message('操作成功',true);
        }else{
            return message('操作失败',false);
        }
    }

    public function show(){
        $param= request()->param();
        $where[]=['id','=',$param['id']];
//        状态   0待审核已投递 1：hr面试2主管面试3总经理面试4通过5不通过
        $info =Db::name('user_resume')->where($where)->find();
        $info['show'] = 1;
       if($info['status'] >=1 && $info['status'] < 5){
           if($info['status1'] >1|| $info['status2']>1|| $info['status3']>1|| $info['status4']>1){
               $info['show'] = 0;
           }
           $return_list[]=[
               'content'=>'面试邀约，面试中-HR面试',
               'remark'=>$info['remark1'],
               'status'=>$info['status1']
           ];
           $return_list[]=[
               'content'=>'面试中-应聘部门主管面试',
               'remark'=>$info['remark2'],
               'status'=>$info['status2']
           ];
           $return_list[]=[
               'content'=>'面试中-总经理面试',
               'remark'=>$info['remark3'],
               'status'=>$info['status3']
           ];
           $return_list[]=[
               'content'=>'面试通过发送offer',
               'remark'=>$info['remark4'],
               'status'=>$info['status4']
           ];

       }elseif($info['status'] == 0){
           $return_list[]=[
               'content'=>'已投递，面试邀约',
               'remark'=>'',
               'status'=>0
           ];
           $info['show'] = 0;
       }else{
           $return_list[]=[
               'content'=>'暂不录用',
               'remark'=>$info['remark5'],
               'status'=>2,
           ];
           $info['show'] = 0;
       }
       $info['return_list']= $return_list;

        return message('',true,$info);





    }

    public function updateEvaluation($admin_id){
        $param= request()->param();
        $where[]=['id','=',$param['id']];
        $where[]=['status','=',$param['resume_status']];
        $data=[
            "remark{$param['resume_status']}"=>$param['remark'],
            "status{$param['resume_status']}"=>$param['status'],
            "update_time{$param['resume_status']}"=>time(),


        ];
        if(in_array($param['resume_status'],[1,2,3])){
            $data['status']=$param['resume_status']+1;
        }
        $res = Db::name('user_resume')->where($where)->update($data);
        if($res){
            return message('操作成功',true);
        }else{
            return message('操作失败',false);
        }

    }
}