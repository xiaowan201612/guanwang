<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\service;


use app\model\Job;
use app\model\Occupation;
use think\facade\Db;

/**
 * 招聘岗位管理-服务类
 * @author 牧羊人
 * @since: 2021/07/20
 * Class JobService
 * @package app\admin\service
 */
class JobService extends BaseService
{
    /**
     * 构造函数
     * LevelService constructor.
     */
    public function __construct()
    {
        $this->model = new Job();
    }
	
	/**
     * 获取数据列表
     * @return array
     * @since 2021/07/20
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->param();

        // 查询条件
        $map = [];


        if (isset($param['mark']) && is_numeric($param['mark'])) {
            $map[] = ['mark', '=', $param['mark']];
        }else{
            $map[] = ['mark', '=', 1];
        }

	
	    // 招聘
        $name = isset($param['name']) ? trim($param['name']) : '';
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }
        $sort = 'id desc';
        $result = $this->model->where($map)->order($sort)->page(PAGE, PERPAGE)->column("id");
        $list = [];
        if (is_array($result)) {
            foreach ($result as $val) {
                $info = $this->model->getInfo($val);
                $list[] = $info;
            }
        }

        //获取数据总数
        $count = $this->model->where($map)->count();
//         $res=parent::getList($map); // TODO: Change the autogenerated stub


        $occupationModel = new Occupation();
        foreach ($list as $key=>$val){
            $occupation_type = $occupationModel->hasWhere('occupationType',[['mark','=',1]],'*','left')->with('occupationType')->where('Occupation.id',$val['occupation_id'])->find();
            $occupation_type=json_decode(json_encode($occupation_type),true);
            $list[$key]['occupation_type'] = empty($occupation_type)?'':$occupation_type['occupationType']['name'].'-'.$occupation_type['name'];
            $list[$key]['city']= Db::name('job_city')->where('id',$val['city_id'])->value('city');
            $list[$key]['status_name']= $val['status']==1?'在招':'停招';
        }

        //返回结果
        $message = array(
            "msg" => '操作成功',
            "code" => 0,
            "data" => $list,
            "count" => $count,
        );
        return $message;
    }

    public function openJob($admin_id){
        $param= request()->param();
        $update=[
            'status'=>$param['status'],
            'update_user'=>$admin_id,
            'update_time'=>time()
        ];
        $res = Db::name('job')->where('id',$param['id'])->update($update);
        if($res){
            return message('操作成功',true);
        }else{
            return message('操作失败',false);
        }
    }

    public function back($admin_id){
        $param= request()->param();
        $update=[
            'mark'=>1,
            'update_user'=>$admin_id,
            'update_time'=>time()
        ];
        $res = Db::name('job')->where('id',$param['id'])->update($update);
        if($res){
            return message('操作成功',true);
        }else{
            return message('操作失败',false);
        }
    }



                            
}