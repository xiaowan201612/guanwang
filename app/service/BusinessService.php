<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\service;


use app\model\Business;
use think\facade\Db;

/**
 * 业务场景管理-服务类
 * @author 牧羊人
 * @since: 2021/07/15
 * Class BusinessService
 * @package app\admin\service
 */
class BusinessService extends BaseService
{
    protected  $return_data=[
    ];
    /**
     * 构造函数
     * LevelService constructor.
     */
    public function __construct()
    {
        $this->model = new Business();
        $this->return_data=[
            'code'=>0,
            'msg'=>'success',
            'data'=>[],
            'count'=>0
        ];
    }

    public function getBusinessList(){
        $param= request()->param();
        $where=[];
        $where[]=['mark','=',1];
        if(isset($param['type']) && !empty($param['type'])){
            $where[]=['type','=',$param['type']];
        }else{
            $where[]=['type','=',1];
        }
        $count  = $this->model->with('businessApply')->where($where)->count();

       $list = $this->model->with('businessApply')->where($where)->order('id desc')->select();
        $list = json_decode(json_encode($list),true);
        foreach ($list as $key=>$value){
            $value['apply_list']= empty($value['businessApply'])?'':implode('/',array_column($value['businessApply'],'name'));
            $list[$key]=$value;
        }
        $this->return_data['count']=$count;
        $this->return_data['data']= $list;
        return $this->return_data;


    }

    public function  getInfo(){
        $param= request()->param();
        $where=[];
        if(!isset($param['id']) || empty($param['id'])){
            $this->return_data['code']=-1;
            $this->return_data['msg']='参数错误';
            return $this->return_data;
        }
        $where[]=['Business.id','=',$param['id']];
        $info = $this->model->hasWhere('businessApply',[],'*','left')->with('businessApply')->where($where)->find();
        $info = json_decode(json_encode($info),true);

        if(!empty($info)){
            if(!empty($info['businessApply'])){
                foreach ($info['businessApply'] as $key=>$value){
                    $value['url']= $value['logo_image'];
                    unset($value['logo_image']);
                    $info['businessApply'][$key]= $value;
                }
            }
            $info['create_time']= empty($info['create_time'])?'':date('Y-m-d H:i:s',$info['create_time']);
            $info['update_time']= empty($info['update_time'])?'':date('Y-m-d H:i:s',$info['update_time']);
        }
        $this->return_data['data']=$info;
        return $this->return_data;

    }
    public function editBusiness($admin_id)
    {

        $param= request()->param();
        $business_data=[
            'name'=>$param['name'],
            'content'=>$param['content'],
            'type'=>isset($param['type'])?$param['type']:1,
        ];
        $apply_list=[];
        if(!empty($param['businessApply'])) {
            foreach ($param['businessApply'] as $key => $value) {
                if(empty($value['logo_image'])){
                    continue;
                }
                $apply_list[] = [
                    'logo_image' => isset($value['logo_image'])?$value['logo_image']:'',
                    'name' => $value['name'],
                    'slogan' => $value['slogan'],
                    'description' => $value['description'],
                    'create_user' => $admin_id,
                    'create_time' => time()
                ];

            }
        }
        if(isset($param['id']) && !empty($param['id'])){

            $business_data['update_user']= $admin_id;
            $business_data['update_time'] = time();
            $delete= \think\facade\Db::name('business_apply')->where('business_id',$param['id'])->delete();
            $update= $this->model->where('id',$param['id'])->update($business_data);

        }else{
            $business_data['update_user']= $admin_id;
            $business_data['update_time'] = time();
            $business_data['create_user']= $admin_id;
            $business_data['create_time'] = time();
            $delete=1;
            $update= $this->model->save($business_data);

        }

        $id= isset($param['id']) && !empty($param['id'])?$param['id']:$this->model->id;
        if(!empty($apply_list)){
            foreach ($apply_list as $key=>$value){
                $apply_list[$key]['business_id']= $id;
            }
            $insert= \think\facade\Db::name('business_apply')->insertAll($apply_list);
        }
        $this->return_data['msg']='操作成功';
        return $this->return_data;
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2021/07/15
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->param();

        // 查询条件
        $map = [];
		
	
	    // 场景名称
        $name = isset($param['name']) ? trim($param['name']) : '';
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }


	
        $res = parent::getList($map); // TODO: Change the autogenerated stub
        $list=$res['data'];
        foreach ($list as $key=>$value){
            $apply_list= \think\facade\Db::name('business_apply')->where('business_id',$value['id'])->select();
            $list[$key]['businessApply']= $apply_list;
        }

        $res['data']= $list;
        return  $res;

    }

    public function comCustomCase()
    {
        $param = request()->param();

        // 查询条件
        $map = [];


        // 场景名称
        $name = isset($param['name']) ? trim($param['name']) : '';
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }
        $count = Db::name('company_custom_case')->where('mark',1)->count();
        $list = Db::name('company_custom_case')->where('mark',1)->order('id desc')->select()->toArray();
        $type=[1=>'app',2=>'小程序',3=>'网站',4=>'MG动画',5=>'宣传片'];
        foreach ($list as $key=>$value){
            $list[$key]['type_name']=isset($type[$value['type']])?$type[$value['type']]:'';
        }
        $this->return_data['data']= $list;
        $this->return_data['count']= $count;
        return  $this->return_data;

    }


    public function demandCustom(){
        $param= request()->param();

        $where=[];
        $where[]=['mark','=',1];
        if(isset($param['date_type']) && !empty($param['date_type'])){
            switch ($param['date_type']){
                case 1:
                    //今日
                    $start_time = mktime(0,0,0,date('m'),date('d'),date('Y'));
                    $end_time = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
                    break;
                case 2:
                    //昨日
                    $start_time = mktime(0,0,0,date('m'),date('d')-1,date('Y'));
                    $end_time = mktime(0,0,0,date('m'),date('d'),date('Y'))-1;
                    break;
                case 3:
                    //php获取本周周起始时间戳和结束时间戳
                    $start_time=mktime(0,0,0,date('m'),date('d')-date('w')+1,date('Y'));
                    $end_time=mktime(23,59,59,date('m'),date('d')-date('w')+7,date('Y'));
                    break;

                case 4:
                    //php获取本月起始时间戳和结束时间戳
                    $start_time=mktime(0,0,0,date('m'),1,date('Y'));
                    $end_time=mktime(23,59,59,date('m'),date('t'),date('Y'));
                    break;
                default:
                    $start_time = mktime(0,0,0,date('m'),date('d'),date('Y'));
                    $end_time = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
                    break;

            }
            $where[] = ['create_time', '>', $start_time];
            $where[] = ['create_time', '<=', $end_time];

        }else {
            if (isset($param['start_time']) && !empty($param['start_time'])) {
                $where[] = ['create_time', '>', strtotime($param['start_time'])];
            }
            if (isset($param['end_time']) && !empty($param['end_time'])) {
                $where[] = ['create_time', '<=', strtotime($param['end_time'] . ' 23:59:59')];
            }
        }
        $count  = Db::name('demand_custom')->where($where)->count();
        $list =Db::name('demand_custom')->where($where)->order('id desc')->select();
        $list = json_decode(json_encode($list),true);
        foreach ($list as $key=>$value){
            $list[$key]['is_file']= empty($value['file_url'])?'无':'有';
            $list[$key]['status_name']= $value['status']==1?'未处理':'已处理';
        }
        $this->return_data['count']=$count;
        $this->return_data['data']= $list;
        return $this->return_data;
    }

    public function demandInfo(){
        $param= request()->param();
        $where=[];
        if(!isset($param['id']) || empty($param['id'])){
            $this->return_data['code']=-1;
            $this->return_data['msg']='参数错误';
            return $this->return_data;
        }
        $where[]=['id','=',$param['id']];
        $info =Db::name('demand_custom')->where($where)->find();
        $info = json_decode(json_encode($info),true);
        if(!empty($info)){
            $info['create_time']= empty($info['create_time'])?'':date('Y-m-d H:i:s',$info['create_time']);
            $info['update_time']= empty($info['update_time'])?'':date('Y-m-d H:i:s',$info['update_time']);
        }
        $this->return_data['data']=$info;
        return $this->return_data;
    }

    public function saveDemand($admin_id){
        $param = request()->param();
        $data=[
            'status'=>$param['status'],
            'update_user'=>$admin_id,
            'update_time'=>time(),
        ];
        Db::name('demand_custom')->where('id',$param['id'])->update($data);
        $this->return_data['msg']='操作成功';
        return $this->return_data;
    }

    public function customCaseInfo(){
        $param= request()->param();
        $where=[];
        if(!isset($param['id']) || empty($param['id'])){
            $this->return_data['code']=-1;
            $this->return_data['msg']='参数错误';
            return $this->return_data;
        }
        $where[]=['id','=',$param['id']];
        $info = Db::name('company_custom_case')->where($where)->find();
        $info = json_decode(json_encode($info),true);
        $this->return_data['data']=$info;
        return $this->return_data;
    }

    public function deleteCustom(){
        $param= request()->param();
        $where=[];
        if(!isset($param['id']) || empty($param['id'])){
            $this->return_data['code']=-1;
            $this->return_data['msg']='参数错误';
            return $this->return_data;
        }
        $where[]=['id','=',$param['id']];

        $info = Db::name('company_custom_case')->where($where)->update(['mark'=>0,'update_time'=>time()]);
        $this->return_data['msg']='操作成功';
        return $this->return_data;
    }

    public function edit()
    {
        return parent::edit(); // TODO: Change the autogenerated stub
    }


}