<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\service;


use app\model\CompanyCase;
use think\Db;

/**
 * 业务场景管理-服务类
 * @author 牧羊人
 * @since: 2021/07/15
 * Class CompanyCaseService
 * @package app\admin\service
 */
class CompanyCaseService extends BaseService
{
    protected  $return_data=[
    ];
    /**
     * 构造函数
     * LevelService constructor.
     */
    public function __construct()
    {
        $this->model = new CompanyCase();
        $this->return_data=[
            'code'=>0,
            'msg'=>'success',
            'data'=>[],
            'count'=>0
        ];
    }

    public function getCompanyCaseList(){
        $param= request()->param();
        $where=[];
        $where[]=['mark','=',1];
        $count  = $this->model->with('companyCaseDetails')->where($where)->count();

       $list = $this->model->with('companyCaseDetails')->where($where)->order('id desc')->select();
        $list = json_decode(json_encode($list),true);
        foreach ($list as $key=>$value){
            $value['apply_list']= empty($value['companyCaseDetails'])?'':implode('/',array_column($value['companyCaseDetails'],'name'));
            $list[$key]=$value;
        }
        $this->return_data['count']=$count;
        $this->return_data['data']= $list;
        return $this->return_data;


    }

    public function  getInfo(){
        $param= request()->param();
        $where=[];
        if(!isset($param['id']) || empty($param['id'])){
            $this->return_data['code']=-1;
            $this->return_data['msg']='参数错误';
            return $this->return_data;
        }
        $where[]=['id','=',$param['id']];
        $info = $this->model->with('companyCaseDetails')->where($where)->find();
        $info = json_decode(json_encode($info),true);
        if(!empty($info)){
            if(!empty($info['companyCaseDetails'])){
                foreach ($info['companyCaseDetails'] as $key=>$value){
                    $value['url']= $value['logo_image'];
                    unset($value['logo_image']);
                    $info['companyCaseDetails'][$key]= $value;
                }
            }
            $info['create_time']= empty($info['create_time'])?'':date('Y-m-d H:i:s',$info['create_time']);
            $info['update_time']= empty($info['update_time'])?'':date('Y-m-d H:i:s',$info['update_time']);
        }
        $this->return_data['data']=$info;
        return $this->return_data;

    }
    public function editCompanyCase($admin_id)
    {

        $param= request()->param();
        $CompanyCase_data=[
            'name'=>$param['name'],
            'content'=>$param['content'],
            'image'=>$param['image']
        ];
        $apply_list=[];
        if(!empty($param['companyCaseDetails'])) {
            foreach ($param['companyCaseDetails'] as $key => $value) {
                if(empty($value['logo_image'])){
                    continue;
                }
                $apply_list[] = [
                    'logo_image' => isset($value['logo_image'])?$value['logo_image']:'',
                    'name' => $value['name'],
                    'transfer_url' => $value['transfer_url'],
                    'create_user' => $admin_id,
                    'create_time' => time()
                ];

            }
        }
        if(isset($param['id']) && !empty($param['id'])){

            $CompanyCase_data['update_user']= $admin_id;
            $CompanyCase_data['update_time'] = time();
            $delete= \think\facade\Db::name('company_case_details')->where('case_id',$param['id'])->delete();
            $update= $this->model->where('id',$param['id'])->update($CompanyCase_data);

        }else{
            $CompanyCase_data['update_user']= $admin_id;
            $CompanyCase_data['update_time'] = time();
            $CompanyCase_data['create_user']= $admin_id;
            $CompanyCase_data['create_time'] = time();
            $delete=1;
            $update= $this->model->save($CompanyCase_data);

        }

        $id= isset($param['id']) && !empty($param['id'])?$param['id']:$this->model->id;
        if(!empty($apply_list)){
            foreach ($apply_list as $key=>$value){
                $apply_list[$key]['case_id']= $id;
            }
            $insert= \think\facade\Db::name('company_case_details')->insertAll($apply_list);
        }
        $this->return_data['msg']='操作成功';
        return $this->return_data;
    }

    /**
     * 获取数据列表
     * @return array
     * @since 2021/07/15
     * @author 牧羊人
     */
    public function getList()
    {
        $param = request()->param();

        // 查询条件
        $map = [];
		
	
	    // 场景名称
        $name = isset($param['name']) ? trim($param['name']) : '';
        if ($name) {
            $map[] = ['name', 'like', "%{$name}%"];
        }


	
        $res = parent::getList($map); // TODO: Change the autogenerated stub
        $list=$res['data'];
        foreach ($list as $key=>$value){
            $apply_list= \think\facade\Db::name('company_case_details')->where('case_id',$value['id'])->select();
            $list[$key]['companyCaseDetails']= $apply_list;
        }

        $this->return_data['data']= $list;
        return  $this->return_data;

    }


                                
}