<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\controller;


use app\service\InvestorsService;
use app\controller\Backend;

/**
 * 媒体及投资者管理-控制器
 * @author 牧羊人
 * @since: 2021/07/17
 * Class Investors
 * @package app\admin\controller
 */
class Investors extends Backend
{
    /**
     * 初始化方法
     * @author 牧羊人
     * @since: 2021/07/17
     */
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->service = new InvestorsService();
    }

    public function feedback(){
        $res = $this->service->feedback($this->userId);
        return $res;
    }
	
                                            
}