<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace app\controller;


use app\service\JobCityService;
use app\controller\Backend;
use think\facade\Db;


/**
 * 招聘城市品管理-控制器
 * @author 牧羊人
 * @since: 2021/07/20
 * Class Jobcity
 * @package app\admin\controller
 */
class Jobcity extends Backend
{
    /**
     * 初始化方法
     * @author 牧羊人
     * @since: 2021/07/20
     */
    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->service = new JobCityService();
    }
    public function edit()
    {
        $params= request()->param();
        if(isset($params['id']) &&  empty($params['id'])){
            $info = Db::name('job_city')->where('city',$params['city'])->where([['id','<>',$params['id']]])->select()->toArray();

        }else{
            $info = Db::name('job_city')->where('city',$params['city'])->select()->toArray();

        }
        if(!empty($info)){
            return message('该城市已添加',false);
        }
        return parent::edit(); // TODO: Change the autogenerated stub
    }

    public function  openCity(){
        $res= $this->service->openCity($this->userId);
        return $res;

    }



    public function province_list(){

        $new_where=[];
        if(!empty($where)){
            if(isset($where['pid'])){
                $new_where[]=['id','=',$where['pid']];
            }

        }
        $result = Db::name('province')->field('id as pid,name')->where($new_where)->where(['status'=>1])->order('capital asc')->select();
        if($result){
            return json_encode($result);
        }else{
            return [];
        }
    }

    public function city_list(){

        $new_where=[];
        if(!empty($where)){
            if(isset($where['cid'])){
                $new_where[]=['id','=',$where['cid']];
            }

        }
        $pid = $this->request->param('pid');
        if(empty($pid)){
            $result = Db::name('city')->field('id as cid,name')->where($new_where)->where(['status'=>1])->order('capital asc')->select();
        }else{

            $result = Db::name('city')->field('id as cid,name')->where($new_where)->where(['status'=>1,'father_id'=>$pid])->order('capital asc')->select();
        }
        if($result){
            return json_encode($result);
        }else{
            return [];
        }
    }
	
                                
}